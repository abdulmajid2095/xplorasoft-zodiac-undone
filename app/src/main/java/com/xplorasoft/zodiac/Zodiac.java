package com.xplorasoft.zodiac;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.LinearLayout;

public class Zodiac extends AppCompatActivity implements View.OnClickListener {

    LinearLayout aquarius, pisces, aries, taurus;
    LinearLayout gemini, cancer, leo, virgo;
    LinearLayout libra, scorpio, sagitarius, capricorn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        setContentView(R.layout.activity_zodiac);

        aquarius = findViewById(R.id.aquarius);
        pisces = findViewById(R.id.pisces);
        aries = findViewById(R.id.aries);
        taurus = findViewById(R.id.taurus);
        gemini = findViewById(R.id.gemini);
        cancer = findViewById(R.id.cancer);
        leo = findViewById(R.id.leo);
        virgo = findViewById(R.id.virgo);
        libra = findViewById(R.id.libra);
        scorpio = findViewById(R.id.scorpio);
        sagitarius = findViewById(R.id.sagitarius);
        capricorn = findViewById(R.id.capricorn);

        aquarius.setOnClickListener(this);
        pisces.setOnClickListener(this);
        aries.setOnClickListener(this);
        taurus.setOnClickListener(this);
        gemini.setOnClickListener(this);
        cancer.setOnClickListener(this);
        leo.setOnClickListener(this);
        virgo.setOnClickListener(this);
        libra.setOnClickListener(this);
        scorpio.setOnClickListener(this);
        sagitarius.setOnClickListener(this);
        capricorn.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId())
        {
            case R.id.aquarius:
                redirect(0);
                break;

            case R.id.pisces:
                redirect(1);
                break;

            case R.id.aries:
                redirect(2);
                break;

            case R.id.taurus:
                redirect(3);
                break;

            case R.id.gemini:
                redirect(4);
                break;

            case R.id.cancer:
                redirect(5);
                break;

            case R.id.leo:
                redirect(6);
                break;

            case R.id.virgo:
                redirect(7);
                break;

            case R.id.libra:
                redirect(8);
                break;

            case R.id.scorpio:
                redirect(9);
                break;

            case R.id.sagitarius:
                redirect(10);
                break;

            case R.id.capricorn:
                redirect(11);
                break;
        }
    }

    public void redirect(int opsi)
    {
        Intent intent = new Intent(Zodiac.this, Zodiac_Detail.class);
        intent.putExtra("opsi",""+opsi);
        startActivity(intent);
    }
}
