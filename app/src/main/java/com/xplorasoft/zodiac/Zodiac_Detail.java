package com.xplorasoft.zodiac;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.daimajia.easing.linear.Linear;

public class Zodiac_Detail extends AppCompatActivity implements View.OnClickListener {

    ImageView gambar;
    TextView nama;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        setContentView(R.layout.activity_zodiac__detail);

        String[] resourceNama = new String[]{""+getResources().getString(R.string.name0), ""+getResources().getString(R.string.name1),
                ""+getResources().getString(R.string.name2), ""+getResources().getString(R.string.name3),
                ""+getResources().getString(R.string.name4),""+getResources().getString(R.string.name5),
                ""+getResources().getString(R.string.name6), ""+getResources().getString(R.string.name7),
                ""+getResources().getString(R.string.name8), ""+getResources().getString(R.string.name9),
                ""+getResources().getString(R.string.name10), ""+getResources().getString(R.string.name11)};

        int[] resourceGambar = new int[]{R.drawable.aquarius,  R.drawable.pisces, R.drawable.aries, R.drawable.taurus, R.drawable.gemini,
                R.drawable.cancer, R.drawable.leo, R.drawable.virgo, R.drawable.libra, R.drawable.scorpio,
                R.drawable.sagitarius, R.drawable.capricorn};

        gambar = findViewById(R.id.gambar);
        nama = findViewById(R.id.nama);

        int opsi = 0;
        try {
            opsi = Integer.parseInt(""+getIntent().getStringExtra("opsi"));
        } catch (NumberFormatException e) {
            e.printStackTrace();
        }

        gambar.setBackgroundResource(resourceGambar[opsi]);
        nama.setText(resourceNama[opsi]);

    }

    @Override
    public void onClick(View view) {

    }


}
